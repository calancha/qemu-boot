# qemu-boot

Create and run an AMD64 Linux filesystem image using QEMU that will
print "hello world" after successful startup.

## Run

Run the script as follows.
```bash
chmod +x boot.sh
sudo ./boot.sh
```

After the startup, the script prints "hello world" and starts a sh
session.
