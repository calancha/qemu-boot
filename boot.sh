#!/bin/bash

#----------------------------#
# Run it as follows:         #
#                            #
# $ sudo ./boot.sh           #
#----------------------------#

# Create a new directory for the filesystem
mkdir -p myfs

# Create a new file for the filesystem
dd if=/dev/zero of=myfs.img bs=1M count=64

# Format the filesystem
mkfs.ext4 myfs.img

# Mount the filesystem
mount -o loop myfs.img myfs

# Install busybox
apt-get install -y busybox-static \
                   qemu-system-x86

# Create the directory structure
mkdir -p myfs/{bin,etc,dev,lib,lib64,proc,root,sbin,sys,tmp,usr/{bin,sbin}}

# Create device nodes
mknod myfs/dev/console c 5 1
mknod myfs/dev/null c 1 3

# Copy busybox binary
cp /usr/bin/busybox myfs/bin/busybox

pushd myfs/bin
# Create symlinks for busybox
for cmd in sh ls cat echo mount umount rm cp mv; do
  ln -s busybox ${cmd}
done
popd

# Create the init script to display the "Hello World!" message and
# start a sh session
cat <<EOF > myfs/init
#!/bin/sh
echo "hello world"
exec /bin/sh
EOF
chmod +x myfs/init

# Generate the initramfs
pushd myfs
find . -print0 | cpio --null -ov --format=newc | gzip -9 > ../initramfs.cpio.gz
popd

# Unmount the filesystem
umount myfs

# Run QEMU with the new filesystem and init script
qemu-system-x86_64 -drive file=myfs.img,format=raw \
     -m 1024M \
     -kernel /boot/vmlinuz-$(uname -r) \
     -initrd initramfs.cpio.gz \
     -append "root=/dev/vda console=ttyS0" \
     -nographic
